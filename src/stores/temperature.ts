import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import axios from 'axios'
import https from '@/services/https'
import { useLoadingStore } from './loading'
import temperatureService from '@/services/temperature'

export const useTemperatureStore = defineStore('temperature', () => {
  const valid = ref(false)
  const celsius = ref(0)
  const result = ref(0)
  const loadingStore = useLoadingStore()


  async function callConvert() {
    //result.value = convert(celsius.value)
    loadingStore.doLoad()
    try {
      result.value = await temperatureService.convert(celsius.value)
    }
    catch (err) {
      console.log(err)
    }
    loadingStore.finish()
  }

  return {
    valid, result, celsius,
    callConvert
  }
})
