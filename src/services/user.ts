import type { User } from "@/types/User"
import https from "./https"


function addUser(user: User): Promise<number> {
    return https.post('/users', user)
}

function updateUser(user: User): Promise<number> {
    return https.patch(`/users/${user.id}`, user)
}

function delUser(user: User): Promise<number> {
    return https.delete(`/users/${user.id}`)
}

function getUser(id: number) {
    return https.get(`/users/${id}`)
}


function getUsers() {
    return https.get('/users')
}

export default { addUser, updateUser, delUser, getUsers, getUser }